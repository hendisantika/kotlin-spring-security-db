# Kotlin Spring Security Database

This is an example about Spring Security from database account using Spring Boot Kotlin

#### Run locally

```
git clone https://gitlab.com/hendisantika/kotlin-spring-security-db.git
```

```
mvn clean spring-boot:run
```

#### Screen shot

Login Page
```
Use this : naruto/naruto
```

![Login page](img/login.png "Login page")

Successful Page

![Successful Page](img/success.png "Successful Page")
