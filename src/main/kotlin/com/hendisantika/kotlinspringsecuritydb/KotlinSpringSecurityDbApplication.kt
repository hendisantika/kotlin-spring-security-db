package com.hendisantika.kotlinspringsecuritydb

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class KotlinSpringSecurityDbApplication

fun main(args: Array<String>) {
    runApplication<KotlinSpringSecurityDbApplication>(*args)
}
